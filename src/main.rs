extern crate iron;
#[macro_use] extern crate mime;

use iron::prelude::*;
use iron::status;

extern crate router;
use router::Router;


use std::fs::File;
use std::io::prelude::*;

fn main() {
    let mut router = Router::new();

    router.get("/", get_form, "root");

    println!("Serving on http://localhost:3000");
    Iron::new(router).http("localhost:3000").unwrap();
    
    let mut file = File::open("templates/document.ms")
        .expect("Can't open file.");

    let mut contents = String::new();

    file.read_to_string(&mut contents)
        .expect("Can't read file.");

    println!("File Contents:\n{}", contents);

    let mut file_to_write = File::create("out.ms")
        .expect("Can't open file");
    
    file_to_write.write_all(contents.as_bytes())
        .expect("Can't write to file.");

}

fn get_form(_request: &mut Request) -> IronResult<Response> {
    let mut response = Response::new();

    response.set_mut(status::Ok);
    response.set_mut(mime!(Text/Html; Charset=Utf8));
    response.set_mut(r#"
<title>Goggle Drive</title>
<center><h1>Goggle Drive</h1></center>
<center><p>A bad ripoff of Google Drive</p></center>

"#);
    Ok(response)
}

